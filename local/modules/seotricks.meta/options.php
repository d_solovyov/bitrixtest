<?
use Bitrix\Main\Localization\Loc;
use	Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

$request = HttpApplication::getInstance()->getContext()->getRequest();

$module_id = htmlspecialcharsbx($request["mid"] != "" ? $request["mid"] : $request["id"]);

Loader::includeModule($module_id);

$aTabs = array(
	array(
		"DIV" 	  => "edit",
		"TAB" 	  => Loc::getMessage("SEOTRICKS_META_OPTIONS_TAB_NAME"),
		"TITLE"   => Loc::getMessage("SEOTRICKS_META_OPTIONS_TAB_NAME"),
		"OPTIONS" => array(
			Loc::getMessage("SEOTRICKS_META_OPTIONS_TAB_COMMON"),
			array(
				"switch_on",
				Loc::getMessage("SEOTRICKS_META_OPTIONS_TAB_SWITCH_ON"),
				"Y",
				array("checkbox")
			),
			array(
				"path",
				Loc::getMessage("SEOTRICKS_META_OPTIONS_TAB_PATH"),
				"#^/catalog/#",
				array("text", 50)
			),
			array(
				"title",
				Loc::getMessage("SEOTRICKS_META_OPTIONS_TAB_TITLE"),
				"#H1#",
				array("text", 50)
			),
			array(
				"browser_title",
				Loc::getMessage("SEOTRICKS_META_OPTIONS_TAB_BROWSER_TITLE"),
				"#TITLE#",
				array("text", 50)
			)
		)
	)
);

$tabControl = new CAdminTabControl(
	"tabControl",
	$aTabs
);

$tabControl->Begin();
?>
<form action="<? echo $APPLICATION->GetCurPageParam() ?>" method="post">

	<?
	foreach($aTabs as $aTab){

		if($aTab["OPTIONS"]){

			$tabControl->BeginNextTab();

			__AdmSettingsDrawList($module_id, $aTab["OPTIONS"]);
		}
	}

	$tabControl->Buttons();
	?>

	<input type="submit" name="apply" value="<? echo(Loc::GetMessage("SEOTRICKS_META_OPTIONS_INPUT_APPLY")); ?>" class="adm-btn-save" />
	<input type="submit" name="default" value="<? echo(Loc::GetMessage("SEOTRICKS_META_OPTIONS_INPUT_DEFAULT")); ?>" />

	<?
	echo(bitrix_sessid_post());
	?>

</form>
<?
$tabControl->End();
?>
<?
if($request->isPost() && check_bitrix_sessid()){

	foreach($aTabs as $aTab){

		foreach($aTab["OPTIONS"] as $arOption){

			if(!is_array($arOption)){

				continue;
			}

			if($arOption["note"]){

				continue;
			}

			if($request["apply"]){

				$optionValue = $request->getPost($arOption[0]);

				if($arOption[0] == "switch_on"){

					if($optionValue == ""){

						$optionValue = "N";
					}
				}

				Option::set($module_id, $arOption[0], is_array($optionValue) ? implode(",", $optionValue) : $optionValue);
			}elseif($request["default"]){

				Option::set($module_id, $arOption[0], $arOption[2]);
			}
		}
	}

	LocalRedirect($APPLICATION->GetCurPageParam());
}
