<?php


namespace Seotricks\Meta;

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Page\Asset;


class Page
{
    public static function process()
    {
        if (!defined("ADMIN_SECTION") && $ADMIN_SECTION !== true) {
            global $APPLICATION;
            $module_id = pathinfo(dirname(__DIR__))["basename"];

            if (Option::get($module_id, "switch_on", "Y")==="Y")
            {
                $path =  Option::get($module_id, "path");
                if ($path)
                {
                    if (!preg_match($path, Application::getInstance()->getContext()->getRequest()->getRequestUri()))
                    {
                        return false;
                    }
                }
                if ($h1 = Option::get($module_id, "title"))
                {
                    $h1 = str_replace("#H1#", $APPLICATION->GetTitle(), $h1);
                    $APPLICATION->SetTitle($h1);
                }
                if ($browser_title = Option::get($module_id, "browser_title"))
                {
                    $browser_title = str_replace("#TITLE#", $APPLICATION->GetPageProperty("title"), $browser_title);
                    $APPLICATION->SetPageProperty("title", $browser_title);
                }
            }
        }
        return false;
    }

}
