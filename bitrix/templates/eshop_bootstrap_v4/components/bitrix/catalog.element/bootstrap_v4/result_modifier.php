<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

if ($COLOR_CODE = $arParams["COLOR_CODE"])
{
    $arOffersUrls = $arOfferColors = array();
    foreach ($arResult['OFFERS'] as $arOffer)
    {
        if ($arOffer["PROPERTIES"]["COLOR_REF"]["VALUE"] == $COLOR_CODE)
        {
            if (!$arResult["OFFER_ID_SELECTED"])
                $arResult["OFFER_ID_SELECTED"] = $arOffer['ID'];
        }
        $arOfferColors[$arOffer['ID']] = $arOffer["PROPERTIES"]["COLOR_REF"]["VALUE"];
        $arOfferUrl[$arOffer['ID']] = str_replace("#COLOR_CODE#", $arOffer["PROPERTIES"]["COLOR_REF"]["VALUE"], $arResult["DETAIL_PAGE_URL"]);
    }
    $arResult["DETAIL_PAGE_URL"] = str_replace("#COLOR_CODE#", $COLOR_CODE, $arResult["DETAIL_PAGE_URL"]);
}
$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$arColors = array();

foreach ($arResult["SKU_PROPS"]["COLOR_REF"]["VALUES"] as $arValue)
{
    if ($arValue["XML_ID"] == $COLOR_CODE)
    {
        $color = strtolower($arValue["NAME"]);
        $arResult["META_TAGS"]["TITLE"] .= " " .$color;
        $arResult["META_TAGS"]["BROWSER_TITLE"] = str_replace("#COLOR#", $color, $arResult["META_TAGS"]["BROWSER_TITLE"]);
    }
    $arColors[$arValue["XML_ID"]] = $arValue["NAME"];
}

foreach ($arResult['JS_OFFERS'] as &$arOffer) {
    $arOffer["DETAIL_PAGE_URL"] = $arOfferUrl[$arOffer["ID"]];
    $arOffer["TITLE"] = $arOffer["NAME"] . " " . $arColors[$arOfferColors[$arOffer["ID"]]];
}
unset($arOfferUrl, $arColors, $arOfferColors);