<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();
foreach ($arResult['ITEMS'] as &$arItem)
{
    $COLOR_CODE = '';
    foreach ($arItem["OFFERS"] as $arOffer)
    {
        if ($arOffer["CAN_BUY"])
        {
            $COLOR_CODE = $arOffer["PROPERTIES"]["COLOR_REF"]["VALUE"];
            break;
        }
    }
    $arItem["DETAIL_PAGE_URL"] = str_replace("#COLOR_CODE#", $COLOR_CODE, $arItem["DETAIL_PAGE_URL"]);
}